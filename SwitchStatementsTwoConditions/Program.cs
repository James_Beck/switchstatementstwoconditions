﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchStatementsTwoConditions
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Direction for the user
            Console.WriteLine("Input a lowercase only colour of your choice");
            ///Input colour that the user has chosen
            var colour = Console.ReadLine();

            switch(colour)
            {
                ///first colour choice is blue
                case "blue":
                    Console.WriteLine($"{colour} is the colour of the sapphire gem");
                    break;
                ///second colour choice is red
                case "red":
                    Console.WriteLine($"{colour} is the colour of the blood in your body");
                    break;
                ///the program considers everything else to be wrong
                default:
                    Console.WriteLine($"This is an executive decision, {colour} is not a good enough colour choice");
                    break;
            }

        }
    }
}
